declpkg - Including packages with dependencies
==============================================

The `declpkg` package includes packages with user provided order.

Installation
------------

The package is supplied in `.dtx` format.
To unpack the `.dtx`, running `tex` will extract the package and
`latex` will extract it and also typeset the documentation.

The package requires LaTeX3 support.
